package maksim.nazarov.testtaskforngs.presentation.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class Converter {

    public static String convertCostForView(Integer cost) {
        if (cost == null || cost == 0) return "цена не указана";
        else return String.format(new Locale("pl", "PL"), "%,d", cost) + " руб.";
    }

    public static String convertFullTimeFormatToDateFormat(String date) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        DateFormat targetFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date newDate = null;
        try {
            newDate = originalFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(newDate);
    }
}
