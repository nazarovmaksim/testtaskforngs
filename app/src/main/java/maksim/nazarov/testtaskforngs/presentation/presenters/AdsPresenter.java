package maksim.nazarov.testtaskforngs.presentation.presenters;

import java.util.List;

import maksim.nazarov.testtaskforngs.data.source.remote.model.Advert;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Linked;
import maksim.nazarov.testtaskforngs.presentation.ui.BaseView;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public interface AdsPresenter extends BasePresenter {
    interface View extends BaseView{
        void showAds(List<Advert> list);
        void addLinked(Linked linked);
    }

    void loadFirstPage();
    void loadAds(int limit, int offset);
}