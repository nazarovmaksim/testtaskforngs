package maksim.nazarov.testtaskforngs.presentation.ui;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public interface BaseView {
    void showProgress();
    void hideProgress();
    void showError(String message);
}
