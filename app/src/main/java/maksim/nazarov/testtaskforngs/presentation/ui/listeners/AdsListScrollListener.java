package maksim.nazarov.testtaskforngs.presentation.ui.listeners;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import maksim.nazarov.testtaskforngs.presentation.presenters.AdsPresenter;

/**
 * Created by Maksim Nazarov on 12.07.2017.
 */

public class AdsListScrollListener extends RecyclerView.OnScrollListener {

    private int mPastVisiblesItems, mVisibleItemCount, mTotalItemCount;

    private Integer mPage = 0;
    private final Integer COUNT_ADS_IN_PAGE = 20;

    private LinearLayoutManager mLayoutManager;
    private AdsPresenter mPresenter;

    public AdsListScrollListener(LinearLayoutManager layoutManager, AdsPresenter adsPresenter) {
        this.mLayoutManager = layoutManager;
        this.mPresenter = adsPresenter;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            mVisibleItemCount = recyclerView.getChildCount();
            mTotalItemCount = mLayoutManager.getItemCount();
            mPastVisiblesItems = mLayoutManager.findLastVisibleItemPosition();
            if (mVisibleItemCount + mPastVisiblesItems >= mTotalItemCount) {
                mPresenter.loadAds(COUNT_ADS_IN_PAGE, COUNT_ADS_IN_PAGE * mPage++);
            }
        }
    }
}
