package maksim.nazarov.testtaskforngs.presentation.presenters.impl;

import com.google.gson.JsonSyntaxException;

import java.util.List;

import maksim.nazarov.testtaskforngs.data.source.remote.model.Advert;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Linked;
import maksim.nazarov.testtaskforngs.domain.repository.AdsDataSource;
import maksim.nazarov.testtaskforngs.presentation.presenters.AdsPresenter;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class AdsPresenterImpl implements AdsPresenter {

    private AdsPresenter.View view;
    private AdsDataSource repository;

    private Boolean isCanLoadAds = true;

    public AdsPresenterImpl(AdsPresenter.View view, AdsDataSource repo) {
        this.view = view;
        this.repository = repo;
    }

    @Override
    public void loadFirstPage() {
        loadAds(20, 0);
    }

    @Override
    public void loadAds(int limit, int offset) {
        if (isCanLoadAds) {
            view.showProgress();
            isCanLoadAds = !isCanLoadAds;
            repository.getAds(limit, offset, new AdsDataSource.LoadAdsCallback() {
                @Override
                public void onAdsLoaded(List<Advert> ads, Linked linked) {
                    view.addLinked(linked);
                    view.showAds(ads);
                    isCanLoadAds = !isCanLoadAds;
                    view.hideProgress();
                }

                @Override
                public void onDataNotAvailable(Throwable throwable) {
                    if(throwable instanceof JsonSyntaxException) {
                        view.showError("Произошла ошибка при загрузке элемента списка");
                    }
                    throwable.printStackTrace();
                    isCanLoadAds = !isCanLoadAds;
                    view.hideProgress();
                }
            });
        }
    }
}
