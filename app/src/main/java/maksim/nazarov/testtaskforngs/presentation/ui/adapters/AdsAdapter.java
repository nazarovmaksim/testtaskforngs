package maksim.nazarov.testtaskforngs.presentation.ui.adapters;

import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import maksim.nazarov.testtaskforngs.R;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Advert;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Linked;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Upload;
import maksim.nazarov.testtaskforngs.presentation.converter.Converter;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.AdHolder> {

    private ArrayList<Advert> mList;
    private Map<String, String> mTags = new HashMap<>();
    private Map<String, Upload> mUploads = new HashMap<>();

    private Context mContext;

    public AdsAdapter(Context mContext) {
        this.mList = new ArrayList();
        this.mContext = mContext;
    }

    public void addAdverts(List<Advert> items) {
        mList.addAll(items);
        notifyItemRangeChanged(mList.size() - items.size(), items.size());
    }

    public void addLinked(Linked linked) {
        for (String tag : linked.tags.keySet())
            if (!mTags.keySet().contains(tag))
                mTags.put(tag, linked.tags.get(tag).title);
        for (String upload : linked.uploads.keySet())
            if (!mUploads.keySet().contains(upload))
                mUploads.put(upload, linked.uploads.get(upload));
    }

    @Override
    public AdHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_item_list, parent, false);
        return new AdHolder(view);
    }

    @Override
    public void onBindViewHolder(AdHolder holder, int position) {
        holder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class AdHolder extends RecyclerView.ViewHolder {

        ImageView ivPhoto;
        LinearLayout mLlBackground;
        private TextView mTvType;
        private TextView mTvDate;
        private TextView mTvTitle;
        private TextView mTvPrice;

        public AdHolder(View itemView) {
            super(itemView);

            ivPhoto = (ImageView) itemView.findViewById(R.id.iv_photo);
            mTvType = (TextView) itemView.findViewById(R.id.tv_type);
            mTvDate = (TextView) itemView.findViewById(R.id.tv_date);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mTvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            mLlBackground = (LinearLayout) itemView.findViewById(R.id.ll_background);
        }

        void bind(Advert advert) {
            // advert.shortImages.main and advert.shortImages.main.links.origin.domain maybe null
            try {
                Upload infoPicture = mUploads.get(advert.shortImages.main.links.origin);
                String photoUrl = "http://" + infoPicture.domain + infoPicture.fileName + "." + infoPicture.fileExtension;
                Picasso.with(mContext)
                        .load(photoUrl)
                        .placeholder(R.mipmap.ic_cloud_load)
                        .error(R.drawable.ic_error_black_24dp)
                        .into(ivPhoto);
            } catch (NullPointerException nullException) {
                nullException.printStackTrace();
            }

            mTvTitle.setText(advert.title != null ? advert.title : "");

            mTvDate.setText(Converter.convertFullTimeFormatToDateFormat(advert.updateDate));

            mTvPrice.setText(Converter.convertCostForView(advert.cost));

            if (!advert.links.tags.isEmpty()) {
                String tag = mTags.get(advert.links.tags.get(0));
                mTvType.setText(tag);
            } else mTvType.setText("");
        }
    }
}
