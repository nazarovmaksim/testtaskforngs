package maksim.nazarov.testtaskforngs.presentation.ui.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import maksim.nazarov.testtaskforngs.R;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Advert;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Linked;
import maksim.nazarov.testtaskforngs.presentation.presenters.AdsPresenter;
import maksim.nazarov.testtaskforngs.presentation.presenters.impl.AdsPresenterImpl;
import maksim.nazarov.testtaskforngs.presentation.ui.adapters.AdsAdapter;
import maksim.nazarov.testtaskforngs.presentation.ui.listeners.AdsListScrollListener;
import maksim.nazarov.testtaskforngs.util.Injection;

public class AdsActivity extends AppCompatActivity implements AdsPresenter.View {

    //Views
    private RecyclerView rv_list;
    private ProgressBar progressBar;

    private AdsAdapter adapter;
    private AdsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        setup();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAds(List<Advert> list) {
        adapter.addAdverts(list);
    }

    @Override
    public void addLinked(Linked linked) {
        adapter.addLinked(linked);
    }

    private void setup() {
        adapter = new AdsAdapter(this);
        presenter = new AdsPresenterImpl(this, Injection.provideTasksRepository());

        progressBar = (ProgressBar) findViewById(R.id.pb_progress);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv_list = (RecyclerView) findViewById(R.id.rv_list);
        rv_list.setLayoutManager(layoutManager);
        rv_list.setAdapter(adapter);
        rv_list.addOnScrollListener(new AdsListScrollListener(layoutManager, presenter));
        rv_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //here may be the call ShareActionProvider for share the item list
            }
        });

        presenter.loadFirstPage();
    }
}
