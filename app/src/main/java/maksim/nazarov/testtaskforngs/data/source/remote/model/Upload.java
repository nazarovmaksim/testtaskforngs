package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Maksim Nazarov on 12.07.2017.
 */

public class Upload {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("file_name")
    @Expose
    public String fileName;
    @SerializedName("file_extension")
    @Expose
    public String fileExtension;
    @SerializedName("width")
    @Expose
    public Integer width;
    @SerializedName("height")
    @Expose
    public Integer height;
    @SerializedName("domain")
    @Expose
    public String domain;
}
