package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maksim Nazarov on 12.07.2017.
 */

public class Metadata {
    @SerializedName("total")
    @Expose
    public Integer total;
    @SerializedName("grouped")
    @Expose
    public List<Object> grouped = new ArrayList<Object>();
}
