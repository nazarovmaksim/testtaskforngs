package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class ResponseModel {

    @SerializedName("request")
    @Expose
    private Object request;
    @SerializedName("error")
    @Expose
    private Object error;
    @SerializedName("notices")
    @Expose
    private List<Object> notices = new ArrayList<Object>();
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    @SerializedName("linked")
    @Expose
    public Linked linked;
    @SerializedName("adverts")
    @Expose
    public List<Advert> adverts = new ArrayList<Advert>();
    @SerializedName("code")
    @Expose
    private Integer code;
}
