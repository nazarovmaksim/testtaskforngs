package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class Links {
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("user")
    @Expose
    public String user;
    @SerializedName("tags")
    @Expose
    public List<String> tags = new ArrayList<String>();
}
