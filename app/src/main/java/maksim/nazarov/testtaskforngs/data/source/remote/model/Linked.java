package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class Linked {
    @SerializedName("tags")
    @Expose
    public Map<String,Tag> tags;
    @SerializedName("uploads")
    @Expose
    public Map<String,Upload> uploads;
}
