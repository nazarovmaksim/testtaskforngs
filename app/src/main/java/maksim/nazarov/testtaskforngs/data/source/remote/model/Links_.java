package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class Links_ {
    @SerializedName("origin")
    @Expose
    public String origin;
    @SerializedName("thumb")
    @Expose
    public String thumb;
}
