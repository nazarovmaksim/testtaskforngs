package maksim.nazarov.testtaskforngs.data.source.remote;

import maksim.nazarov.testtaskforngs.App;
import maksim.nazarov.testtaskforngs.data.source.remote.model.ResponseModel;
import maksim.nazarov.testtaskforngs.domain.repository.AdsDataSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Maksim Nazarov on 12.07.2017.
 */

public class RemoteAdsDataSource implements AdsDataSource {

    private static RemoteAdsDataSource INSTANCE;
    private static NgsAPI ngsApi;

    private RemoteAdsDataSource() {
        ngsApi = App.retrofit.create(NgsAPI.class);
    }

    public static RemoteAdsDataSource getInstance() {
        if (INSTANCE == null) INSTANCE = new RemoteAdsDataSource();
        return INSTANCE;
    }

    @Override
    public void getAds(int limit, int offset, final LoadAdsCallback callback) {
        ngsApi.getAds(limit, offset, "uploads,tags", "short_images,cost,update_date").enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                ResponseModel responseBody = response.body();
                callback.onAdsLoaded(responseBody.adverts, responseBody.linked);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                callback.onDataNotAvailable(t);
            }
        });
    }
}
