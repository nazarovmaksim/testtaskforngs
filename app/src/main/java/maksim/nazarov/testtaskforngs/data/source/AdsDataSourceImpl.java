package maksim.nazarov.testtaskforngs.data.source;

import maksim.nazarov.testtaskforngs.domain.repository.AdsDataSource;

/**
 * Created by Maksim Nazarov on 12.07.2017.
 */

public class AdsDataSourceImpl implements AdsDataSource {

    private static AdsDataSourceImpl INSTANCE;

    private AdsDataSource mRemoteAdsDataSource;

    private AdsDataSourceImpl(AdsDataSource remoteAdsDataSource) {
        this.mRemoteAdsDataSource = remoteAdsDataSource;
    }

    public static AdsDataSourceImpl getInstance(AdsDataSource remoteAdsDataSource) {
        if (INSTANCE == null) INSTANCE = new AdsDataSourceImpl(remoteAdsDataSource);
        return INSTANCE;
    }

    @Override
    public void getAds(int limit, int offset, LoadAdsCallback callback) {
        mRemoteAdsDataSource.getAds(limit, offset, callback);
    }
}
