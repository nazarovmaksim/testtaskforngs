package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class Main {
    @SerializedName("links")
    @Expose
    public Links_ links;
    @SerializedName("sequence")
    @Expose
    public Integer sequence;
    @SerializedName("is_main")
    @Expose
    public Boolean isMain;
}
