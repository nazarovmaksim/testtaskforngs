package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class Advert {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("links")
    @Expose
    public Links links;
    @SerializedName("short_images")
    @Expose
    public ShortImages shortImages;
    @SerializedName("cost")
    @Expose
    public Integer cost;
    @SerializedName("update_date")
    @Expose
    public String updateDate;
}
