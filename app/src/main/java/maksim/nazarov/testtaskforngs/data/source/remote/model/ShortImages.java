package maksim.nazarov.testtaskforngs.data.source.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public class ShortImages {
    @SerializedName("total")
    @Expose
    public Integer total;
    @SerializedName("main")
    @Expose
    public Main main;
}
