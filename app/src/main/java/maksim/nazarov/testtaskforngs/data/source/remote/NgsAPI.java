package maksim.nazarov.testtaskforngs.data.source.remote;

import maksim.nazarov.testtaskforngs.data.source.remote.model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public interface NgsAPI {
    @GET("api/v1/adverts/")
    Call<ResponseModel> getAds(@Query("limit") Integer limit,
                               @Query("offset")Integer offset,
                               @Query("include")String include,
                               @Query("fields")String fields);
}
