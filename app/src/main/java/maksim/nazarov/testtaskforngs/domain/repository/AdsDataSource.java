package maksim.nazarov.testtaskforngs.domain.repository;

import java.util.ArrayList;
import java.util.List;

import maksim.nazarov.testtaskforngs.data.source.remote.model.Advert;
import maksim.nazarov.testtaskforngs.data.source.remote.model.Linked;

/**
 * Created by Maksim Nazarov on 11.07.2017.
 */

public interface AdsDataSource {

    interface LoadAdsCallback{
        void onAdsLoaded(List<Advert> ads, Linked linked);
        void onDataNotAvailable(Throwable throwable);
    }

    void getAds(int limit, int offset, LoadAdsCallback callback);
}
