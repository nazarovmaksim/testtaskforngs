package maksim.nazarov.testtaskforngs;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nazarov Maksim on 11.07.2017.
 */

public class App extends Application {

    public static Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://do.ngs.ru/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
