package maksim.nazarov.testtaskforngs.util;

import android.content.Context;
import android.support.annotation.NonNull;

import maksim.nazarov.testtaskforngs.data.source.AdsDataSourceImpl;
import maksim.nazarov.testtaskforngs.data.source.remote.RemoteAdsDataSource;
import maksim.nazarov.testtaskforngs.domain.repository.AdsDataSource;

/**
 * Created by Maksim Nazarov on 12.07.2017.
 */

public class Injection {


    public static AdsDataSource provideTasksRepository() {
        return AdsDataSourceImpl.getInstance(RemoteAdsDataSource.getInstance());
    }
}
